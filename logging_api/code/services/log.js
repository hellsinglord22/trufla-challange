const Log = require('../models/log');
module.exports = {
  create: (databaseEntry) => {
    return Log.create(databaseEntry);
  },
  findByUserId: (userId) => {
    return Log.find({ userId });
  },

}

