const rabbitMQ = require('amqplib/callback_api');
const EventEmitter = require('events');


class MessageBroker extends EventEmitter {

  constructor() {
    super();
    this.rabbitMQConnection = null;
    this.queueCache = [];

    this.EVENTS = {
      CONNECTING: 'CONNECTING',
      RECONNECTING: 'RECONNECTING',
      CONNECTION_ERROR: 'CONNECTION_ERROR',
      CONNECTED: 'CONNECTED'
    }
  }

  connect(connectionURL, options) {

    const { EVENTS } = this;
    rabbitMQ.connect(connectionURL, (connectionError, rabbitMQConnection) => {

      if (connectionError) {
        this.emit(EVENTS.CONNECTION_ERROR, connectionError);
        if (options.autoReconnect) {
          this.reconnect(connectionURL, options);
        }
      }
      else {
        this.rabbitMQConnection = rabbitMQConnection;
        this.emit(EVENTS.CONNECTED, this);
      }

    });
  }


  reconnect(rabbitMQConnection, options) {
    const { EVENTS } = this;
    const { reconnectInterval } = options;
    this.emit(EVENTS.RECONNECTING);
    setTimeout(this.connect.bind(this, rabbitMQConnection, options), reconnectInterval);
  }

  consume(queue, cb) {

    this.rabbitMQConnection.createChannel((error, channel) => {
      if (error) {
        cb(error, null);
      }
      else {
        this.assertQueue(queue, channel);
        channel.consume(queue, (message) => {
          cb(null, message.content.toString());
        });
      }
    })
  }


  produce({ queue, data }, cb) {
    this.rabbitMQConnection.createChannel((error, channel) => {
      if (error) {
        cb(error, null);
        return;
      }
      else {
        this.assertQueue(queue, channel);
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        cb();
      }
    })
  }

  assertQueue(queue, channel) {
    const newQueue = this.queueCache.indexOf(queue) != -1 ? true : false;
    if (!newQueue) {
      channel.assertQueue(queue, {
        durable: false
      });
      this.queueCache.push(queue);
    }
  }

}

module.exports = new MessageBroker();
