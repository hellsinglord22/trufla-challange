const rabbitMQ = require('amqplib/callback_api');
const EventEmitter = require('events');


class Consumer extends EventEmitter {

  constructor({ shouldReconnect = true, connectionURL }) {
    super();
    this.rabbitMQConnection = null;
    this.connectionURL = connectionURL;
    this.shouldReconnect = shouldReconnect;
    this.EVENTS = {
      CONNECTING: 'CONNECTING',
      RECONNECTING: 'RECONNECTING',
      CONNECTION_ERROR: 'CONNECTION_ERROR',
      CONNECTED: 'CONNECTED'
    }
  }

  connect() {

    const { EVENTS, connectionURL } = this;
    rabbitMQ.connect(connectionURL, (connectionError, rabbitMQConnection) => {

      if (connectionError) {
        this.emit(EVENTS.CONNECTION_ERROR, connectionError);
        if (this.shouldReconnect) {
          this.retryConnectingAfterSecond();
        }
      }
      else {
        this.rabbitMQConnection = rabbitMQConnection;
        this.emit(EVENTS.CONNECTED, this);
      }

    });
  }


  retryConnectingAfterSecond() {
    const { EVENTS } = this;
    const SECOND = 1000;
    this.emit(EVENTS.RECONNECTING);
    setTimeout(this.connect.bind(this), SECOND);
  }

  consume(queue, cb) {
    this.rabbitMQConnection.createChannel((error, channel) => {
      if (error) {
        cb(error, null);
        return;
      }
      else {
        channel.assertQueue(queue, {
          durable: false
        })
        channel.consume(queue, (message) => {
          cb(null, message.content.toString());
        });
      }
    })
  }

}


module.exports = {
  create: (options) => {
    return new Consumer(options);
  }
}


