const messageBroker = require('./message_broker');

const notify  = ({ subject, to, content }) => {

  const from = 'truflacodechallange@gmail.com';
  const SEND_NOTIFICATION_QUEUE = 'SEND_NOTIFICATION_QUEUE';
  messageBroker.produce({ queue: SEND_NOTIFICATION_QUEUE, data: {
    subject,
    to,
    content,
    from,
  }}, (error) => {
    console.log(error);
  });

};

module.exports = {
  notify
}