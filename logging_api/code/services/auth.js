const config = require('config');
const jwt = require('jsonwebtoken');

const authenticateUser = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.get('JWT_SECRET'), (error, decoded) => {
      if (error)
        reject(error);
      else
        resolve(decoded.userId);
    })
  });
}


module.exports = {
  authenticateUser
}
