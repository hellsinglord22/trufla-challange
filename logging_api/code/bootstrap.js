const mongoose = require('mongoose');
const config = require('config');
const ApiProblem = require('express-api-problem');
const messageBroker = require('./services/message_broker');
const chalk = require('chalk');
const LogService = require('./services/log');
const NotificationService = require('./services/notification');

const MONGO_CONNECT_OPTIONS = {
  reconnectTries: config.get('MONGO.RECONNECT_TRIES'),
  reconnectInterval: config.get('MONGO.RECONNECT_INTERVAL'),
  useNewUrlParser: true
};

module.exports = {
  registerConnectionLogs: () => {
    return new Promise((resolve) => {
      const MESSAGE_BROKER_EVENTS = messageBroker.EVENTS;
      const MONGOOSE_EVENTS = {
        CONNECTING: 'connecting',
        ERROR: 'error',
        CONNECTED: 'connected'
      };
      messageBroker.on(MESSAGE_BROKER_EVENTS.CONNECTED, () => {
        console.log(`Connected to ${chalk.underline('rabbitMQ')}`);
      });
      messageBroker.on(MESSAGE_BROKER_EVENTS.RECONNECTING, () => {
        console.log(`Failed to connect to ${chalk.underline('rabbitMQ')}, reconnecting ...`);
      });
      mongoose.connection.on(MONGOOSE_EVENTS.CONNECTED, () => {
        console.log(`Connected to ${chalk.underline('mongoDB')}`);
      });
      mongoose.connection.on(MONGOOSE_EVENTS.RECONNECTING, () => {
        console.log(`Failed to connect to ${chalk.underline('mongoDB')}, reconnecting ...`);
      });
      resolve();
    })
  },
  connectToDatabase: () => {
    return new Promise((resolve, reject) => {
      mongoose.connect(config.get('MONGO.CONNECTION_STRING'), MONGO_CONNECT_OPTIONS, (databaseConnectionError) => {
        if (databaseConnectionError) {
          reject(new ApiProblem(500, 'Failed connecting to database', `Failed connecting to database`, {
            message: databaseConnectionError.message
          }));
        }
        else {
          resolve();
        }
      });
    })
  },
  connectToRabbitMQ: () => {
    return new Promise((resolve) => {
      messageBroker.on(messageBroker.EVENTS.CONNECTED, () => {
        resolve();
      })
      messageBroker.connect(config.get('RABBIT_MQ.CONNECTION_URL'), {
        autoReconnect: config.get('RABBIT_MQ.AUTO_RECONNECT'),
        reconnectInterval: config.get('RABBIT_MQ.RECONNECT_INTERVAL')
      });
    });
  },
  registerConsumersToMessageBroker: async () => {
    return new Promise((resolve) => {
      const CREATE_LOG_QUEUE = 'CREATE_LOG_QUEUE';
      messageBroker.consume(CREATE_LOG_QUEUE, (channelCreationError, message) => {
        if (!channelCreationError) {
          LogService.create(JSON.parse(message))
            .then((createdLog) => {
              const content = `<p> Log with content <strong>${createdLog.content}</strong> has been created`;
              const subject = 'Log Creation Notification'
              const emails = config.get('NOTIFICATION_EMAILS');
              NotificationService.notify({
                subject, content, to: emails
              });
            })
        }
      });
      resolve();
    })
  }
}