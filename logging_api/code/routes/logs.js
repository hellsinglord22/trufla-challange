const express = require('express');
const router = express.Router();
const LogService = require('../services/log');

router.get('/', (request, response, next) => {
  const { userId } = request.query;
  LogService.findByUserId(userId)
    .then((allUsers) => {
      response.send(allUsers);
    })
    .catch(next);
});

router.post('/', (request, response, next) => {
  const requestBody = request.body;
  LogService.create(requestBody)
    .then((createdLog) => {
      response.send(createdLog)
    })
    .catch(next);
});

module.exports = router;
