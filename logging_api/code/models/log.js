const mongoose = require('mongoose');
const schema = new mongoose.Schema({
  content: {
    type: 'string',
    required: true
  },
  userId: {
    type: 'string',
    required: true
  }
},{
    timestamps: { 
      createdAt: 'created_at',
      updatedAt: 'updated_at' 
    }
});
module.exports = mongoose.model('Log', schema);