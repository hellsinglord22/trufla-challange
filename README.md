# Trufla Task API

## Content
1. How to use ?
2. Routes
3. Assumptions
4. Architecture decisions
5. Coding decisions
6. If only there was more time

---

### How to use ?
- Install **docker** as per this [installation guide](https://docs.docker.com/compose/install/) .
- Install **GIT** as per this [installation guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- clone the Repo using this command `git clone https://gitlab.com/hellsinglord22/trufla-challange` .
- run the app using `docker-compose up` or `docker-compose up -d` to run in the background .
- If ou want to get notification on creating a log, please update the `logging_api/code/config/default.json`, `NOTIFICATION_EMAILS` attribute .
- Swagger documentation will be served on `localhost:3000/docs` or if you want to use our [postman collection](https://www.getpostman.com/collections/cff6272aa8a002ebd601) be my guest .


---

### Assumption
I have made the assumption that whoever will review this project will want to go throw it as fast as possible, that's why i have made this application to be as portable as possible so decision like using `default.json` that contains all the keys needed wouldn't happen in a production ready version of this app, also i would have made different version of the docker file for production and staging .
For Development purposes i have exposed ports to the database and internal service, in production these services is not exposed . 

--- 

### Routes
|     Endpoint     	| Method 	|                                           Description                                           	|
|:----------------:	|:------:	|:-----------------------------------------------------------------------------------------------:	|
| `/auth/register` 	| `POST` 	| create new api user                                                                             	|
| `/auth/login`    	| `POST` 	| login using created user credentials, returns token to be used later to consume other Endpoints 	|
| `/logs`          	| `POST` 	| Create new log                                                                                  	|
| `/logs`          	| `GET`  	| List user created logs, only return logs created by the logged in user                          	|


---

### Architecture decisions 
- I have started by creating monolithic application `application_api` that work as `gateway`, it contains **application authentication** and finally **user creation route**, in a project like Trufla's true connect i will devide this into 2 services and use `nginx` as a gateway .
- `logging_api` have both endpoints and consumer running in the background, consumers is registered in the `bootstrap` file .
- `logging_api` connections established in the `app` which is connections to `rabbitMQ`, `mongoDB` .
- `logging_api` register connection status logs at the `bootstrap` file .
- `mailing_service` only accepts `payloads` via **rabbitMQ** .
- Each `api` database is entirely separated which means database is only accessed by one `api` .
- Database schema is simple.
- `application_api` is the only public .
 ![](images/architecture.png)

 ---

### Coding decisions
- Wrap most `npm` packages and create an interface for them with an abstract name for example when i wrapped `rabbitMQ` lib i didn't call the wrapper `rabbitMQ` instead i have called it `messageBroker`.
- I have `test` files next to `module` i am testing .
- Abstract object creation make it easier to stub dependencies and if a module needs to have the responsibility of creating other dependencies i create a factory and dependency inject it to that module .
- Single level of abstraction per function .
- `Tests` works as a `README` to the `Application`, `unit test` to me is the best documentation and example on how to use already written functions, check `mailer.spec.js` .
- I setup Test dependencies in the `test-setup.spec.js` file .


### If only i have time
- Increase test coverage / remove duplications wether syntax duplication or implementation duplication .
- Add performance hocks and snap shot monitoring to find memory leaks and increase performance . 
- Use for this APIs, node native API / fastify to squeeze extra thousand request per second .
- Refactoring `mailing_service` to be consistent with other endpoints.
- Add `ping` and `status` endpoints .
- Add `eslint` .
- Wrap `console.log` .
