const auth = require('../services/auth');
const ApiProblem = require('express-api-problem');

module.exports = (request, response, next) => {
  const token = request.headers['x-auth-token'];
  if (!token) {
    next(new ApiProblem(400, 'x-auth-token is required for auth'));
  }
  else {
    auth.authenticateUser(token)
      .then((userId) => {
        request.userId = userId;
        next(null);
      })
      .catch(() => {
        next(new ApiProblem(401, 'Access forbidden'));
      });
  }
}