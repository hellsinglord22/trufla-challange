const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bootstrap = require('./bootstrap');
const authUser = require('./middlewares/auth-middleware');
const errorHandlerMiddleware  =require('./middlewares/error-handler-middleware');
const swaggerDocument = require('./documents/swagger.json');
const swaggerUi = require('swagger-ui-express');


const logesRouter = require('./routes/logs');
const authRouter = require('./routes/auth');

const app = express();

// view engine setup

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/auth', authRouter);
app.use('/logs', authUser, logesRouter);
app.use((errorHandlerMiddleware));


bootstrap.connectToDatabase()
  .then(() => {
    console.log('database connection established !!!');
  })
  .catch(console.error);



module.exports = app;
