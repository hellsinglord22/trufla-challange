const mongoose = require('mongoose');
const config = require('config');
const ApiProblem = require('express-api-problem');

module.exports = {
  connectToDatabase: () => {
    return new Promise((resolve, reject) => {
      mongoose.connect(config.get('MONGO.CONNECTION_STRING'), {
        reconnectTries: config.get('MONGO.RECONNECT_TRIES'),
        reconnectInterval: config.get('MONGO.RECONNECT_INTERVAL'),
        useNewUrlParser: true
      }, (databaseConnectionError) => {
        if (databaseConnectionError) {
          reject(new ApiProblem(500, 'Failed connecting to database', `Failed connecting to database`, {
            message: databaseConnectionError.message
          }));
        }
        else {
          resolve();
        }
        return;
      });
    })
  },
  kickStartProducerConnection: () => {
  }
}