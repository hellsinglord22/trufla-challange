const express = require('express');
const router = express.Router();
const UserService = require('../services/user');
const AuthService = require('../services/auth');

router.post('/register', (request, response, next) => {
  const requestBody = request.body;
  UserService.create(requestBody)
    .then((created_user) => {
      response.send(created_user);
    }).catch(next);
});

router.post('/login', (request, response, next) => {
  const requestBody = request.body;
  AuthService.login(requestBody)
    .then((token) => {
      response.send(token);
    }, next);
});

module.exports = router;
