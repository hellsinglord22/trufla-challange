const express = require('express');
const router = express.Router();
const LogService = require('../services/log');

router.get('/', (request, response, next) => {
  const { userId } = request;
  LogService.findByUserId(userId)
    .then((userCreatedLogs) => {
      response.send(userCreatedLogs)
    })
    .catch(next);
});

router.post('/', (request, response, next) => {
  const requestBody = request.body;
  const { userId } = request;
  LogService.create({ userId, ...requestBody })
    .then(() => {
      response.send({ message: 'message produced' })
    })
    .catch(next);
});

module.exports = router;
