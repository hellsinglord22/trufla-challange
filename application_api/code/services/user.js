const User = require('../models/user');
const EncryptionAlgorithm = require('./encryption');

module.exports = {
  create: async (databaseEntry) => {
    const encryptedPassword =  await EncryptionAlgorithm.encrypt(databaseEntry.password)
    const clonedDatabaseEntry = {
      email: databaseEntry.email,
      password: encryptedPassword
    }
    return await User.create(clonedDatabaseEntry);
  },
  findByEmail: (email) => {
    return User.findOne({
      email
    });
  },
}