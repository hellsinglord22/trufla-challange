const bcrpyt = require('bcryptjs');
const { expect } = require('chai');
const EncryptionAlgorithms = require('./encryption');

describe('EncryptionAlgorithms', () => {
  describe('encrypt(val: string)', () => {

    it('Expects wrapper/interface to call bcrpyt hash function once', async function () {
      const bcryptHashFunction = this.sandbox.spy(bcrpyt, 'hash');
      await EncryptionAlgorithms.encrypt('abc');

      expect(bcryptHashFunction).to.have.been.calledOnce;
    });

    it('Expect encrypt function to resolve a string', async function() {
      const encryptedString = await EncryptionAlgorithms.encrypt('abc');
      console.log(encryptedString);
      expect(encryptedString).to.be.string;
    });

  });

  describe('compare(encryptedValue: string, value: string)', () => {

    it('Expect to return truthfully - happy path', async function () {
      const encryptedData = '$2b$10$VHKjefZZRAbqf0SI4vwkRebP0ukJt1z6I9YaSW64EXZcKT16vA3hy';
      const data = 'abc';
      const isSame = await EncryptionAlgorithms.compare(data, encryptedData);

      expect(isSame).to.be.true;
    });

    it('Expect to return false - happy path', async function () {
      const encryptedData = '$2b$10$VHKjefZZRAbqf0SI4vwkRebP0ukJt1z6I9YaSW64EXZcKT16vA3hy';
      const data = 'abcd';
      const isSame = await EncryptionAlgorithms.compare(data, encryptedData);

      expect(isSame).to.be.false;
    });

    it('Expects wrapper/interface to call bcrpyt compare function once', async function () {
      const bcryptCompareFunction = this.sandbox.spy(bcrpyt, 'compare');
      await EncryptionAlgorithms.compare('abc');

      expect(bcryptCompareFunction).to.have.been.calledOnce;
    });
  });

})