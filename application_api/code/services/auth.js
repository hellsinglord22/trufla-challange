const UserService = require('./user');
const EncryptionAlgorithm = require('./encryption');
const JWT = require('./jwt');;
const ApiProblem = require('express-api-problem');

const authenticateUser = async (token) => {
  try {
    const userId = await JWT.verify(token);
    return userId;
  }
  catch (invalidToken) {
    throw invalidJWTToken();
  }
}

const login = async ({ email, password }) => {
  const loggedInUser = await UserService.findByEmail(email);
  if (!loggedInUser) {
    throw invalidEmailOrPassword();
  }
  const isValidPassword = await EncryptionAlgorithm.compare(password, loggedInUser.password);
  if (isValidPassword) {
    const token = await JWT.sign( { userId: loggedInUser._id });
    return {
      token
    }
  }
  else {
    throw invalidEmailOrPassword();
  }
};

const invalidJWTToken = () => {
  return new ApiProblem(401, 'invalid jwt token', 'access denied, invalid JWT token');
};


const invalidEmailOrPassword = () => {
  return new ApiProblem(400, 'invalid email/password');
};

module.exports = {
  authenticateUser,
  login
}
