const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = {
  verify: (token) => {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.get('JWT_SECRET'), (error, decoded) => {
        if (error)
          reject(error);
        else
          resolve(decoded.userId);
      })
    });
  },

  sign: (payload) => {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, config.get('JWT_SECRET'), (error, token) => {
        if (error) {
          reject(error);
        }
        else {
          resolve(token);
        }
      })
    });
  }

}

