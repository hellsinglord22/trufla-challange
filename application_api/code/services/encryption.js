const bcrypt = require('bcryptjs')
const config = require('config');

module.exports = {
  encrypt: (val) => {
    return new Promise((resolve, reject) => {
      bcrypt.hash(val, config.get('SALT'), (error, encryptedString) => {
        if (error) {
          reject(error);
        }
        else {
          resolve(encryptedString);
        }
      });
    })
  },
  compare: (data, encryptedData) => {
    return new Promise((resolve) => {
      bcrypt.compare(data, encryptedData, (error, same) => {
        resolve(!error && same);
      })
    })
  }
}