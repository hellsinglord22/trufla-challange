const { expect } = require('chai');
const JWT = require('./jwt');
const jwt = require('jsonwebtoken');


describe('JWT', () => {
  describe('sign(payload: any)', () => {

    it('Success in calling jwt api', async function() {
      const jwtSignFunction = this.sandbox.spy(jwt, 'sign');
      await JWT.sign({ userId: 1 });
      expect(jwtSignFunction).to.have.been.calledOnce;
    });

    it('Success in signing/verifying JWT Token', async function() {
      const token = await JWT.sign({ userId: 1 });
      const userId = await JWT.verify(token);
      expect(userId).equal(1);
    });


  })

  describe('verify(token: string)', () => {

    it('Success in calling jwt api', async function() {
      const jwtSignFunction = this.sandbox.spy(jwt, 'verify');
      await JWT.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE1NTc3OTM5OTJ9.YRtCxK67yvrS-2AyG9P9BWmTYu0Ug7Z4m2PzFGR6JAk');
      expect(jwtSignFunction).to.have.been.calledOnce;
    });

    it('Success in verifying token', async function() {
      const userId = await JWT.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE1NTc3OTM5OTJ9.YRtCxK67yvrS-2AyG9P9BWmTYu0Ug7Z4m2PzFGR6JAk');
      expect(userId).equal(1);
    });

  });


});