const config = require('config');
const axios = require('axios');
const querystring = require('querystring');
const Producer = require('./producer').create({
  connectionURL: config.get('RABBIT_MQ.CONNECTION_URL')
});
const CREATE_LOG_QUEUE = 'CREATE_LOG_QUEUE';
const { EVENTS } = Producer;


Producer.on(EVENTS.CONNECTED, () => {
  console.log("Application api is connected");
})
Producer.connect();


module.exports = {
  create: (databaseEntry) => {
    return new Promise((resolve, reject) => {
      Producer.produce({ queue: CREATE_LOG_QUEUE, data: databaseEntry }, (error) => {
        if (error)
          reject(error);
        else
          resolve();
      });
    })
  },
  findByUserId: (userId) => {
    const url = config.get('LOG_API.CONNECTION_URL') + '/logs' + '?' + querystring.encode({userId});
    return axios.get(url)
      .then(response => response.data);
  },
}