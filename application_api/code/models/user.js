const mongoose = require('mongoose');
const schema = new mongoose.Schema({
  email: {
    type: 'string',
    unique: true
  },
  password: 'string'
},{
    timestamps: { 
      createdAt: 'created_at',
      updatedAt: 'updated_at' 
    }
});
module.exports = mongoose.model('User', schema);