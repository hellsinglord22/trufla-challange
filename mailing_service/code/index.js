const config = require('config');
const { CONNECTION_URL } = config.get('QUEUE');
const consumer = require('./helper/consumer').create({ connectionString: CONNECTION_URL });
const { EVENTS } = consumer;
const SEND_NOTIFICATION_QUEUE = 'SEND_NOTIFICATION_QUEUE';
const mailer = require('./helper/mailer');

consumer.on(EVENTS.CONNECTED, (connection) => {
  console.log('Congratulations you are connected');
  connection.consume(SEND_NOTIFICATION_QUEUE, (error, messages) => {
    if (error) {
      console.log('Error consuming');
    }
    else {
      mailer.sendMail(messages);
    }
  })
});

consumer.on(EVENTS.RECONNECTING, () => {
  console.log('Reconnecting ...');
});

consumer.on(EVENTS.CONNECTION_ERROR, (connectionError) => {
  console.log('Connection Error ...');
});

consumer.connect();