const { expect } = require('chai');
const mailer = require('./mailer'); 

describe('mailer', () => {
  describe('sendMail({from: string, to: string, subject: string, content: string})', () => {

    it('Mails one address - happy path', async function () {
      const info = await mailer.sendMail({
        from: 'trufla@zoho.com',
        to: 'bar@example.com',
        content: '<p>A log enter was created</p>',
        subject: 'notification'
      });
      return expect(info.envelope).deep.equal({
        "from": "trufla@zoho.com",
        "to": [
          "bar@example.com"
        ]
      });
    });

    it('Mails multiple addresses', async function () {
      const info = await mailer.sendMail({
        from: 'trufla@zoho.com',
        to: 'bar@example.com, baz@example.com',
        content: '<p>A log enter was created</p>',
        subject: 'notification'
      });
      return expect(info.envelope).deep.equal({
        "from": "trufla@zoho.com",
        "to": [
          "bar@example.com",
          "baz@example.com"
        ]
      });
    });

  })
});
