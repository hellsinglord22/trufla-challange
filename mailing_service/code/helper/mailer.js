const nodemailer = require('nodemailer');
const config = require('config');
const stubTransport = require('nodemailer-stub-transport');
const mailerConfiguration = config.get('mailer');
const node_env = config.get('node_env');

const sendMail = async ({from, to, subject, content}) => {
  const transporter = nodemailer.createTransport(_createMailerConfigurationObject());
  const info = await transporter.sendMail({
    from,
    to,
    subject,
    html: content
  })
  return info;
}

const _createMailerConfigurationObject = () => {
  return node_env === 'test' ? stubTransport() : mailerConfiguration;
};

module.exports = {
  sendMail
}
