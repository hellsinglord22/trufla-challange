const rabbitMQ = require('amqplib');
const winston = require('winston');
const config = require('config');
const queueSystemConfigurations = config.get('QUEUE');
const SECOND = 1000;


const connectToQueueSystem = async(trailNumber = 1) => {
  let rabbitMQConnection;
  const { CONNECTION_URL, RETRY_COUNT } = queueSystemConfigurations;
  if (trailNumber > RETRY_COUNT) {
    throw new Error(`Failed to connect to rabbitMQ after ${RETRY_COUNT} trails !!!\n`);
  }
  try {
    console.log('Connecting to rabbitMQ ...');
    rabbitMQConnection = await rabbitMQ.connect(CONNECTION_URL);
  }
  catch(error) {
    console.log('Failed Connecting, retrying');
    await wait(SECOND);
    rabbitMQConnection = await connectToQueueSystem(trailNumber + 1);
  }
  return rabbitMQConnection;
};


const wait = (timeInMillisecond) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
      return;
    }, timeInMillisecond);
  });
};

const bootstrapLogger = () => {
  winston.setLevels({
    debug: 5,
    info: 4,
    warning: 3,
    error: 2,
    critical: 1,
    test: 0
  });
  winston.addColors({
    debug: 'green',
    info: 'cyan',
    warn: 'yellow',
    error: 'red',
    critical: 'red',
    test: 'blue'
  });
  winston.remove(winston.transports.Console);
  winston.add(winston.transports.Console, {
    level: config.get('logger_level'),
    colorize: true
  });
}


module.exports = {
  bootstrapLogger,
  connectToQueueSystem
}


/// create logger and helper functions and add test coverage for them